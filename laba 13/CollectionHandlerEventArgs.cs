﻿using System;
using System.Collections.Generic;
using System.Text;

namespace laba_13
{
    public class CollectionHandlerEventArgs : EventArgs
    {
        public string CollectionName { get; set; }
        public string Type { get; set; }
        public object Object { get; set; }
        public CollectionHandlerEventArgs(string N, string T, object O)
        {
            CollectionName = N;
            Type = T;
            Object = O;
        }
        public override string ToString()
        {
            string result = "";
            result += "//EVENT//\n";
            result += "Имя коллекции - " + CollectionName + "\n";
            result += "Тип изменения - " + Type + "\n";
            result += "Объект        - " + Object.ToString() + "\n";
            result += "//EVENT//\n";
            return result;
        }
    }
}
