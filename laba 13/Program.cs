﻿using System;
using laba_10;
namespace laba_13
{
    class Program
    {
        static Journal j1 = new Journal();
        static Journal j2 = new Journal();
        static void DisplayJ1(object sourse, CollectionHandlerEventArgs e)
        {
            Console.WriteLine(e.ToString());
            JournalEntry je = new JournalEntry(e.CollectionName, e.Type, e.Object.ToString());
            j1.Add(je);
        }
        static void DisplayJ2(object sourse, CollectionHandlerEventArgs e)
        {
            Console.WriteLine(e.ToString());
            JournalEntry je = new JournalEntry(e.CollectionName, e.Type, e.Object.ToString());
            j2.Add(je);
        }

        static void Main(string[] args)
        {
            MyNewCollection<PrintedEdition> collection1 = new MyNewCollection<PrintedEdition>("Library 1");
            MyNewCollection<PrintedEdition> collection2 = new MyNewCollection<PrintedEdition>("Library 2");

            collection1.CollectionCountChanged += DisplayJ1;
            collection1.ElChanged += DisplayJ1;
            collection1.ElChanged += DisplayJ2;
            collection2.ElChanged += DisplayJ2;

            Console.WriteLine("\nСобытия\n");

            collection1.Add(new Book(2003,"A1","N1",350,1));
            collection1.Del();
            collection1.Add(new Magazine(2015,"A2","N2",100,11));
            collection1.SetBegin(new TextBook(2007, "A3", "N3", 1000, 1, "math"));

            collection2.Add(new Book(2003, "A4", "N4", 350, 1));
            collection2.Del();
            collection2.Add(new Magazine(2015, "A5", "N5", 100, 11));
            collection2.SetBegin(new TextBook(2007, "A6", "N6", 1000, 1, "math"));

            Console.WriteLine("\nКоллекция 1\n");
            foreach (PrintedEdition i in collection1) i.ShowObject();

            Console.WriteLine("\nКоллекция 2\n");
            foreach (PrintedEdition i in collection2) i.ShowObject();

            Console.WriteLine("\nЖурнал 1\n");
            for (int i = 0; i < j1.Count; i++) Console.WriteLine(j1[i].ToString());

            Console.WriteLine("\nЖурнал 2\n");
            for (int i = 0; i < j2.Count; i++) Console.WriteLine(j2[i].ToString());
        }
    }
}
