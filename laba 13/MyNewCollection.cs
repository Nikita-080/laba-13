﻿using System;
using System.Collections.Generic;
using System.Text;
using laba_12;
namespace laba_13
{
    public class MyNewCollection<T>: MyCollection<T>
    {
        public string Name { get; set; }
        public delegate void CollectionHandler(object source, CollectionHandlerEventArgs args);
        public event CollectionHandler CollectionCountChanged;
        public event CollectionHandler ElChanged;
        public new void Add(T dat)
        {
            base.Add(dat);
            OnCollectionCountChanged(this, new CollectionHandlerEventArgs(Name,"//ADD//",dat));
        }
        public new void Del()
        {
            T el = Begin;
            base.Del();
            OnCollectionCountChanged(this, new CollectionHandlerEventArgs(Name, "//DEL//", el));
        }
        public new void SetBegin(T dat)
        {
            base.SetBegin(dat);
            OnElChanged(this, new CollectionHandlerEventArgs(Name, "//CHANGE//", dat));
        }


        public virtual void OnCollectionCountChanged(object source, CollectionHandlerEventArgs args)
        {
            if (CollectionCountChanged != null)
                CollectionCountChanged(source, args);
        }
        public virtual void OnElChanged(object source, CollectionHandlerEventArgs args)
        {
            if (ElChanged != null)
                ElChanged(source, args);
        }
        public MyNewCollection() : base()
        {
            Name = "Empty";
        }
        public MyNewCollection(string N) : base()
        {
            Name = N;
        }
    }
}
