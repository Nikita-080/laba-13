﻿using System;
using System.Collections.Generic;
using System.Text;

namespace laba_13
{
    public class JournalEntry
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Data { get; set; }
        public JournalEntry(string N, string T, string D)
        {
            Name = N;
            Type = T;
            Data = D;
        }
        public override string ToString()
        {
            string result = "";
            result += "//JOURNAL SHEET//\n";
            result += "Имя коллекции - " + Name + "\n";
            result += "Тип изменения - " + Type + "\n";
            result += "Данные        - ";
            result += Data.ToString() + "\n";
            result += "//JOURNAL SHEET//\n";
            return result;
        }
    }
}
