using Microsoft.VisualStudio.TestTools.UnitTesting;
using laba_13;
using laba_12;
namespace TestProject13
{
    [TestClass]
    public class UnitTest1
    {
        CollectionHandlerEventArgs data;
        Journal j;
        void TestDisplayEvent(object sourse, CollectionHandlerEventArgs e)
        {
            data = e;
        }
        void TestDisplayJournal(object sourse, CollectionHandlerEventArgs e)
        {
            data = e;
            JournalEntry je = new JournalEntry(e.CollectionName, e.Type, e.Object.ToString());
            j.Add(je);
        }
        [TestMethod]
        public void EventAddChange()
        {
            data = null;

            MyNewCollection<int> collection = new MyNewCollection<int>("Library");
            collection.CollectionCountChanged += TestDisplayEvent;
            collection.Add(3);

            CollectionHandlerEventArgs truedata = new CollectionHandlerEventArgs("Library", "//ADD//", 3);
            
            Assert.AreEqual(data.ToString(),truedata.ToString());
        }
        [TestMethod]
        public void EventDelChange()
        {
            data = null;

            MyNewCollection<int> collection = new MyNewCollection<int>("Library");
            collection.CollectionCountChanged += TestDisplayEvent;
            collection.Add(3);
            collection.Del();

            CollectionHandlerEventArgs truedata = new CollectionHandlerEventArgs("Library", "//DEL//", 3);

            Assert.AreEqual(data.ToString(), truedata.ToString());
        }
        [TestMethod]
        public void Journal()
        {
            data = null;
            j = new Journal();

            MyNewCollection<int> collection = new MyNewCollection<int>();
            collection.CollectionCountChanged += TestDisplayJournal;
            collection.Add(3);

            Journal truejournal = new Journal();
            truejournal.Add(new JournalEntry("Empty", "//ADD//", data.Object.ToString()));

            Assert.AreEqual(j[0].ToString(),truejournal[0].ToString());
        }
        [TestMethod]
        public void JournalCount()
        {
            data = null;
            j = new Journal();

            MyNewCollection<int> collection = new MyNewCollection<int>();
            collection.CollectionCountChanged += TestDisplayJournal;
            collection.Add(3);

            Journal truejournal = new Journal();
            truejournal.Add(new JournalEntry("Empty", "//ADD//", data.Object.ToString()));

            Assert.AreEqual(j.Count, truejournal.Count);
        }
        [TestMethod]
        public void EventChange()
        {
            data = null;

            MyNewCollection<int> collection = new MyNewCollection<int>("Library");
            collection.ElChanged += TestDisplayEvent;
            collection.Add(3);
            collection.SetBegin(4);

            CollectionHandlerEventArgs truedata = new CollectionHandlerEventArgs("Library", "//CHANGE//", 4);

            Assert.AreEqual(data.ToString(), truedata.ToString());
        }
    }
}
